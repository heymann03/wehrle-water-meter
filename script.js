/**
 * Decode function is the main entry point in the script,
 * and you should not delete it.
 *
 * Arguments:
 * 'payload' - an array of bytes, raw payload from sensor
 * 'port' - an integer value in range [0, 255], f_port value from sensor
 */
/*Polyfill for ISO time format with offset*/
Date.prototype.toIsoString = function() {
    var tzo = -this.getTimezoneOffset(),
        dif = tzo >= 0 ? '+' : '-',
        pad = function(num) {
            var norm = Math.floor(Math.abs(num));
            return (norm < 10 ? '0' : '') + norm;
        };
    return this.getFullYear() +
        '-' + pad(this.getMonth() + 1) +
        '-' + pad(this.getDate()) +
        'T' + pad(this.getHours()) +
        ':' + pad(this.getMinutes()) +
        ':' + pad(this.getSeconds()) +
        dif + pad(tzo / 60) +
        ':' + pad(tzo % 60);
}
var hexPayload = '';
var result = new Object();

function portChecking(port) {
    var portsDictionary = {
        '14': 'Water Usage',
        '24': 'Status',
        '50': 'Configuration',
        '99': 'Boot/Debug',
    };
    return portsDictionary[port];
}
/*function for reversing the string*/
function reverseString(str) {
    return str.match(/.{1,2}/g).reverse().join("");
}
/*function for water usage, port - 14, payload = 4 bytes, message = liter count*/
function waterUsage(Payload) {
    var usage = parseInt(reverseString(Payload), 16).toString(10);
    return usage;
}
var date = new Date();

function decode(payload, port) {
    Object.keys(payload).forEach(function(key) {
        thishex = Number(payload[key]).toString(16);
        thishex = thishex.length < 2 ? "" + "0" + thishex : thishex;
        hexPayload += thishex;
    });
    //result.RawPayload = hexPayload;
    if (port == 14) {
        if (hexPayload.length == 8) {
            /*returns water usage value*/
            result.Liters = waterUsage(hexPayload);
            result.RawPayload = hexPayload;
            result.DateTime = date.toIsoString();
            result.Port = port + ':' + portChecking(port);
        }
    } else if (port == 24) {
        if (hexPayload.length == 82) {
            var usageCount = waterUsage(hexPayload.substring(0, 4));
            result.Liters = usageCount;
            var battery = ((parseInt(hexPayload.substring(8, 10), 16).toString(10)) * 0.016);
            /*returns remaining battery value in V*/
            result.Battery = battery + ' V';
            var temperature = parseInt(hexPayload.substring(10, 12), 16).toString(10);
            /*returns temperature value in degree Celcius*/
            result.Temperature = temperature + ' C';
            result.RawPayload = hexPayload;
            result.DateTime = date.toIsoString();
            console.log(portChecking(port));
            result.Port = port + ':' + portChecking(port);
        }
    } else {
        null;
    }
    return result;
}
/*TODO*/
/*1.Implement Byte-8&9*
 *Page-5, payload description*
 *Page-6&7, detailed description of bits*/
