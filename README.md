# Wehrle Water-meter

JavaScript decoder function for Wehrle water-meter

Manufacturer - Wehrle / Andrae

Product name - MTK-HWX Mehrstrahl-Flügelrad-Trockenläufer

## Platform

ThingsHub

## Important links

[https://ecbm.atlassian.net/wiki/spaces/ECBMDoku/pages/43319360/Wehrle+MTK-HWX+Mehrstrahl-Fl+gelrad-Trockenl+ufer?atlOrigin=eyJpIjoiMjc0ZTQzYTUwZjMyNGY2YzhhYTY2MzQ0ODkxNjc2NTEiLCJwIjoiYyJ9]